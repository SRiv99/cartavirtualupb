package com.example.asus.cartavirtualupb;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng poblado = new LatLng(6.210272, -75.571849);
        mMap.addMarker(new MarkerOptions().position(poblado).title("Poblado").snippet("Calle 9 #43 B43").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        LatLng laplaya = new LatLng(6.248179, -75.559401);
        mMap.addMarker(new MarkerOptions().position(laplaya).title("La Playa").snippet("Calle 52 # 39 A 36").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        LatLng la80 = new LatLng(6.218588, -75.598579);
        mMap.addMarker(new MarkerOptions().position(la80).title("La 80").snippet("Diagonal 75 B # 8-14").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        LatLng laureles = new LatLng(6.245348, -75.595504);
        mMap.addMarker(new MarkerOptions().position(laureles).title("Laureles").snippet("Av. Nutibara #74-39").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        LatLng la70 = new LatLng(6.252722, -75.587970);
        mMap.addMarker(new MarkerOptions().position(la70).title("La 70").snippet("Carrera 70 # 47 – 6").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(poblado, 13));
    }
}

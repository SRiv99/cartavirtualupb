package com.example.asus.cartavirtualupb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CartaActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);

        Button entr = (Button)findViewById(R.id.btnEntradas);
        Button pf = (Button)findViewById(R.id.btnPlatosFuertes);
        Button beb = (Button)findViewById(R.id.btnBebidas);
        Button pos = (Button)findViewById(R.id.btnPostres);

        entr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = new Intent(CartaActivity.this,EntradasActivity.class);
                startActivity(int1);
            }
        });

        pf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = new Intent(CartaActivity.this,PlatosFuertesActivity.class);
                startActivity(int1);
            }
        });

        beb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = new Intent(CartaActivity.this,BebidasActivity.class);
                startActivity(int1);
            }
        });

        pos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = new Intent(CartaActivity.this,PostresActivity.class);
                startActivity(int1);
            }
        });
    }
}

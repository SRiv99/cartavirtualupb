package com.example.asus.cartavirtualupb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class EntradasActivity extends Activity {

    int[] imags = {R.drawable.stlouiswings , R.drawable.champinonespesto, R.drawable.piatto, R.drawable.caprese, R.drawable.camembert, R.drawable.berenjenasiliciana, R.drawable.antipastopiatto, R.drawable.patatasbravas, R.drawable.pulpoparrilla, R.drawable.camaronescalamares};
    String[] entradas = {"St. Louis Wings", "Champiñones", "Piatto", "Caprese", "Camembert", "Berenjena Siciliana", "Antipasto Piatto", "Patatas Bravas", "Pulpo a la Parrilla", "Camarones y Calamares"};
    String[] ingr = {"Alas en salsa de la casa Texas Original con papas a la francesa y salsa de ajo", "Salsa Pesto o Salsa de Ajo gratinados con Mozarella y Parmesano", "Tomates y Mozarella, Prosciutto y albahaca", "Tomates confitados, Mozarella de búfala, reducción de balsamico, albahaca", "Fundido el horno con almendra y miel de maple", "Gratinados al horno, en slasa Napolitana, Mozarella y albahaca", "Tomates confitados, alcachofa, balsámico, Mozarella de búfala, Prosciutto, salami", "Papas picantes con salsa de la casa, chorizo español y Parmesano", "Acompañados de papa rústica y tomates confitados", "Sazonados y apanados, acompañados con Salsa Tártara o St. Louis"};
    String[] precios = {"20.9", "22.9", "26.9", "24.9", "26.9", "18.9", "33.9", "19.9", "36.9", "24.9"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entradas);

        ListView listView = (ListView)findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return entradas.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout, null);

            ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_entradas =(TextView)view.findViewById(R.id.textView_comida);
            TextView textView_ingr =(TextView)view.findViewById(R.id.textView_desc);
            TextView textView_precios =(TextView)view.findViewById(R.id.textView_precios);

            imageView.setImageResource(imags[i]);
            textView_entradas.setText(entradas[i]);
            textView_ingr.setText(ingr[i]);
            textView_precios.setText(precios[i]);

            return view;
        }
    }
}

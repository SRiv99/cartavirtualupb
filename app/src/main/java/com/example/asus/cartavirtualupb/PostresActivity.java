package com.example.asus.cartavirtualupb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PostresActivity extends Activity {

    int[] imags = {R.drawable.malteada , R.drawable.brownie, R.drawable.browniehelado, R.drawable.tiramisu, R.drawable.paraiso, R.drawable.delchef};
    String[] postres = {"Malteada de Vainilla", "Brownie Caliente", "Brownie Caliente", "Tiramisú", "Paraíso", "Del Chef"};
    String[] ingr = {"Salsa de Chocolate o Frutos Rojos", "Salsa de Chocolate o Frutos Rojos", "Helado de Vainilla, Salsa de Chocolate, Frutos Rojos o Caramelo", "Original Receta Italiana", "Merenguitos, Helado, Salsa de Chocolate o Frutos Rojos", "Pizza de Galletas con Nutella, Helado Vainilla y Salsa de Chocolate, Frutos Rojos o Caramelo"};
    String[] precios = {"12.9", "6.9", "9.9", "12.9", "13.9", "18.9"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postres);

        ListView listView = (ListView)findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return postres.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout, null);

            ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_entradas =(TextView)view.findViewById(R.id.textView_comida);
            TextView textView_ingr =(TextView)view.findViewById(R.id.textView_desc);
            TextView textView_precios =(TextView)view.findViewById(R.id.textView_precios);

            imageView.setImageResource(imags[i]);
            textView_entradas.setText(postres[i]);
            textView_ingr.setText(ingr[i]);
            textView_precios.setText(precios[i]);

            return view;
        }
    }
}

package com.example.asus.cartavirtualupb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PlatosFuertesActivity extends Activity {

    int[] imags = {R.drawable.hamburguesa, R.drawable.crematomate, R.drawable.sopapiatto, R.drawable.sopacebolla, R.drawable.sopalentejas, R.drawable.sopadimare, R.drawable.cesaranchoas, R.drawable.cesarpollo, R.drawable.cesar, R.drawable.dimare, R.drawable.pizza, R.drawable.lasagna, R.drawable.napolitana, R.drawable.carbonara, R.drawable.bolognesa, R.drawable.salmon, R.drawable.trucha, R.drawable.ossobuco, R.drawable.beef, R.drawable.pechuga, R.drawable.pechugachef, R.drawable.alwok, R.drawable.calentao};
    String[] platos = {"Hamburguesa Especial", "Crema de Tomate", "Sopa Piatto", "Sopa de Cebolla", "Sopa de Lentejas", "Sopa Di Mare", "César Anchoas", "César con Filetes de Pollo", "César", "Di Mare", "Pizza", "Lasagna", "Spaghetti Napolitana", "Spaguetti Carbonara", "Spaguetti", "Salmón a la Plancha", "Trucha Salmonada", "Ossobuco de Ternera", "Beef Paleta", "Pechuga al Horno", "Pechuga del Chef", "Arroz Oriental (Al Wok)", "Arroz Calentao Vasco"};
    String[] ingr = {"Gratinada con Salsa de Champiñones al Vino o Carbonara", "", "Fondo de Carne y Pollo, Vegetales, Albondigas, Huevo", "", "", "Langostinos, Camarones y Calamares", "Ensalada César con anchoas, queso Parmesano y Crotones", "Lechuga Romana, Filetes de Pollo", "", "Calamares y Langostinos, Salsa de ajo, Vinagreta de la casa", "Hawaiana, Pollo y Champiñones, Corn and Bacon, Napolitana, Vegetariana, Pepeperoni, Tocineta", "Pollo, Bolognesa, Mixta, Vegetales", "Albahaca y Parmesano", "Tocineta, Huevo, Parmesano", "Alfredo, Bolognesa, Pesto, Burro", "Salsa Pesto, Naranja o a la Vodka", "Salteada con ajo, mantequilla y perejil, Salsa de la casa", "Cocción lenta, vegetales, especias y vino blanco", "Bañado en salsa de Champiñones al Vino o de Carne y Tocineta", "Miel Mostaza, Pesto o Champiñones al Vino", "Envuelta en Tocineta y Gratinada con Mozzarella y Parmesano", "Lomo de Res, Pollo, Camarón y Vegetales Frescos", "Carne magra desmechada y salteada, frijol rojo en su salsa, platano maduro y tocineta"};
    String[] precios = {"21.9", "12.9", "13.9", "12.9", "13.9", "19.9", "27.9", "25.9", "16.9", "29.9", "27.9", "29.9", "21.9", "23.9", "24.0", "39.9", "38.9", "34.9", "29.9", "26.9", "29.9", "28.9", "24.9"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_platos_fuertes);

        ListView listView = (ListView)findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return platos.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout, null);

            ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_entradas =(TextView)view.findViewById(R.id.textView_comida);
            TextView textView_ingr =(TextView)view.findViewById(R.id.textView_desc);
            TextView textView_precios =(TextView)view.findViewById(R.id.textView_precios);

            imageView.setImageResource(imags[i]);
            textView_entradas.setText(platos[i]);
            textView_ingr.setText(ingr[i]);
            textView_precios.setText(precios[i]);

            return view;
        }
    }
}

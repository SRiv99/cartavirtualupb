package com.example.asus.cartavirtualupb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class BebidasActivity extends Activity {

    int[] imags = {R.drawable.applemartini, R.drawable.applemojito, R.drawable.margaritamerlot, R.drawable.mojito, R.drawable.padrino, R.drawable.vinocaliente,  R.drawable.clubcolombia, R.drawable.corona, R.drawable.aguila, R.drawable.aguilalight, R.drawable.gaseosa, R.drawable.agua, R.drawable.jugos,  R.drawable.jugosleche, R.drawable.limonada,  R.drawable.limonadahierb,  R.drawable.limonadacercoc, R.drawable.zumo, R.drawable.tea, R.drawable.cafeamericano, R.drawable.cappuccino, R.drawable.macchiato, R.drawable.aromatica, R.drawable.espresso, R.drawable.latte, R.drawable.mocaccino, R.drawable.te,};
    String[] bebidas = {"Apple Martini", "Apple Mojito", "Margarita Merlot", "Mojito", "Padrino", "Vino Caliente", "Club Colombia", "Corona", "Águila", "Águila Light", "Gaseosa botella", "Agua", "Jugos de frutas", "Jugos de Frutas", "Limonada Natural", "Limonada", "Limonada", "Zumo de limón", "Ice Tea", "Café Americano", "Cappuccino", "Macchiato", "Aromática", "Espresso", "Café Latte", "Mocaccino", "Té Inglés"};
    String[] ingr = {"Vodka, Apple mix y cereza", "Ron Blanco, Apple mmix, Hierbabuena y soda", "Tequila, limón y vino tinto Merlot", "Ron Blanco, Hierbabuena, soda y limón", "Whisky y Amaretto", "Vino Tinto - Brandy - Jugo de Mandarina - Clavo y Canela, en copa glaseada", "Michelada             10.9", "Michelada             13.9", "", "", "", "", "Mandarina, Mora, Maracuyá, Guanábana, Lulo, Fresa, Mango, Piña y Yerbabuena", "En leche", "", "Hierbabuena", "Cerezada o Coco", "", "Limón, Durazno", "", "", "", "", "", "", "", ""};
    String[] precios = {"22.9", "22.9", "22.9", "20.9", "22.9", "20.9", "7.9", "10.9", "7.5", "6.9", "5.0", "4.9", "5.9", "6.9", "4.9", "5.9", "7.9", "2.9", "4.9", "4.3", "6.5", "5.5", "4.9", "4.3", "5.5", "5.5", "4.3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebidas);

        ListView listView = (ListView)findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return bebidas.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout, null);

            ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_entradas =(TextView)view.findViewById(R.id.textView_comida);
            TextView textView_ingr =(TextView)view.findViewById(R.id.textView_desc);
            TextView textView_precios =(TextView)view.findViewById(R.id.textView_precios);

            imageView.setImageResource(imags[i]);
            textView_entradas.setText(bebidas[i]);
            textView_ingr.setText(ingr[i]);
            textView_precios.setText(precios[i]);

            return view;
        }
    }
}

package com.example.asus.cartavirtualupb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button ubic = (Button)findViewById(R.id.btnUbicacion);
        Button cart = (Button)findViewById(R.id.btnCarta);
        Button resv = (Button)findViewById(R.id.btnReserva);
        Button serv = (Button)findViewById(R.id.btnServicio);

        ubic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = new Intent(MainActivity.this,MapsActivity.class);
                startActivity(int1);
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = new Intent(MainActivity.this,CartaActivity.class);
                startActivity(int1);
            }
        });

        resv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                startActivity(int1);
            }
        });

        serv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1 = new Intent(MainActivity.this,CalificacionActivity.class);
                startActivity(int1);
            }
        });
    }
}
